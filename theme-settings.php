<?php
/**
 * @file
 * Beale Street theme-settings.php
 */

function bealestreet_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {


  $form['page'] = array(
    '#type' => 'details',
    '#title' => t('Page settings'),
    '#open' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['font'] = array(
    '#type' => 'details',
    '#title' => t('Font settings'),
    '#open' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['misc'] = array(
    '#type' => 'details',
    '#title' => t('Misc settings'),
    '#open' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['css'] = array(
    '#type' => 'details',
    '#title' => t('Custom Css'),
    '#open' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['page']['bealestreet_style'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#description' => t('Choose your favorite color.'),
    '#default_value' => theme_get_setting('bealestreet_style', $theme = NULL),
    '#options' => array(
      'blue' => t('Blue'),
      'orange' => t('Orange'),
      'green' => t('Green'),
      'red' => t('Red'),
    ),
  );

  $form['misc']['bealestreet_pickstyle'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enables the StylePicker JS'),
    '#description' => t('If enabled then you can use stylepicker see README.txt.'),
    '#default_value' => theme_get_setting('bealestreet_pickstyle', $theme = NULL),
  );

  $form['page']['bealestreet_fixedwidth'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Fixed Width'),
    '#default_value' => theme_get_setting('bealestreet_fixedwidth', $theme = NULL),
  );

  $form['page']['bealestreet_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Fixed Width Size'),
    '#description' => t('Set the width in Pixel.'),
    '#default_value' => theme_get_setting('bealestreet_width', $theme = NULL),
    '#size' => 5,
    '#maxlength' => 5,
  );

  $form['font']['bealestreet_fontfamily'] = array(
    '#type' => 'select',
    '#title' => t('Font Family'),
    '#description' => t('Choose your favorite Fonts'),
    '#default_value' => theme_get_setting('bealestreet_fontfamily', $theme = NULL),
    '#options' => array(
     'Arial, Verdana, sans-serif' => t('Arial, Verdana, sans-serif'),
     '"Arial Narrow", Arial, Helvetica, sans-serif' => t('"Arial Narrow", Arial, Helvetica, sans-serif'),
     '"Times New Roman", Times, serif' => t('"Times New Roman", Times, serif'),
     '"Lucida Sans", Verdana, Arial, sans-serif' => t('"Lucida Sans", Verdana, Arial, sans-serif'),
     '"Lucida Grande", Verdana, sans-serif' => t('"Lucida Grande", Verdana, sans-serif'),
     'Tahoma, Verdana, Arial, Helvetica, sans-serif' => t('Tahoma, Verdana, Arial, Helvetica, sans-serif'),
     'Georgia, "Times New Roman", Times, serif' => t('Georgia, "Times New Roman", Times, serif'),
     'Custom' => t('Custom (specify below)'),
    ),
  );

  $form['font']['bealestreet_customfont'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Font-Family Setting'),
    '#description' => t('type your fonts separated by ,<br />eg. <b>"Lucida Grande", Verdana, sans-serif</b>'),
    '#default_value' => theme_get_setting('bealestreet_customfont', $theme = NULL),
    '#size' => 40,
    '#maxlength' => 75,
  );

  $form['css']['bealestreet_uselocalcontent'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include Custom Stylesheet'),
    '#default_value' => theme_get_setting('bealestreet_uselocalcontent', $theme = NULL),
  );

  $form['css']['bealestreet_localcontentfile'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to Custom Stylesheet'),
    '#description' => t('type the location of your custom css without leading slash<br />eg. <b>sites/all/themes/newsflash/css/icons.css</b>'),
    '#default_value' => theme_get_setting('bealestreet_localcontentfile', $theme = NULL),
    '#size' => 40,
    '#maxlength' => 75,
  );

  $form['page']['bealestreet_leftsidebarwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Left Sidebar Width'),
    '#description' => t('Set the width in Pixel.'),
    '#default_value' => theme_get_setting('bealestreet_leftsidebarwidth', $theme = NULL),
    '#size' => 5,
    '#maxlength' => 5,
  );

  $form['page']['bealestreet_rightsidebarwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Right Sidebar Width'),
    '#description' => t('Set the width in Pixel.'),
    '#default_value' => theme_get_setting('bealestreet_rightsidebarwidth', $theme = NULL),
    '#size' => 5,
    '#maxlength' => 5,
  );

  $form['misc']['bealestreet_suckerfish'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Suckerfish Menus'),
    '#default_value' => theme_get_setting('bealestreet_suckerfish', $theme = NULL),
  );

  $form['misc']['bealestreet_footerlogo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Ropple Footer Logo'),
    '#description' => t('if unchecked then roople logo in the footer will disapear<br>so you don\'t need touch the code'),
    '#default_value' => theme_get_setting('bealestreet_footerlogo', $theme = NULL),
  );
  return $form;
}
